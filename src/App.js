import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import image from "./assets/images/user.jpg";

function App() {
  return (
    <div>
      <div className='dc-container'>
        <div className='dc-image-container'>
          <img src={image} alt="avatar user" className='dc-image'></img>
        </div>
        <div className='dc-quote'>
          <p>This is one of the best developer blogs on the planet! I read it daily to improve my skills.</p>
        </div>
        <div className='dc-info'>
          <b>Tammy Stevens</b> &nbsp; * &nbsp; Front End Developer
        </div>
      </div>
    </div>
  );
}

export default App;
